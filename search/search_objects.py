from typing import Generator, Optional
import math


class Problem:
    """The abstract class for a formal problem. A new domain subclasses this,
    overriding `actions` and `results`, and perhaps other methods.
    The default heuristic is 0 and the default action cost is 1 for all states.
    When yiou create an instance of a subclass, specify `initial`, and `goal` states
    (or give an `is_goal` method) and perhaps other keyword args for the subclass."""

    def __init__(self, initial=None, goal=None):
        self.initial = initial
        self.goal = goal

    def actions(self, state):
        """Return the actions that can be executed in the given
        state. The result would typically be a list, but if there are
        many actions, consider yielding them one at a time in an
        iterator, rather than building them all at once."""
        raise NotImplementedError

    def result(self, state, action):
        """Return the state that results from executing the given
        action in the given state. The action must be one of
        self.actions(state)."""
        raise NotImplementedError

    def is_goal(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal or checks for state in self.goal. Override this method if
        checking against a single self.goal is not enough."""
        return state == self.goal

    def action_cost(self, s, a, s1):
        """Return the cost of a path that arrives at s1 from/ The default method costs 1
        for every step in the path."""
        return 1

    def h(self, node):
        """Heuristic for informed search algorithms"""
        return 0

    def value(self, state):
        """For optimization problems, each state has a value.  Hill-climbing
        and related algorithms try to maximize this value."""
        raise NotImplementedError

    def __str__(self):
        return '{}({!r}, {!r})'.format(
            type(self).__name__, self.initial, self.goal)


class Node:
    """A node in a search tree. Contains a pointer to the parent (the node
    that this is a successor of) and to the actual state for this node. Note
    that if a state is arrived at by two paths, then there are two nodes with
    the same state. Also includes the action that got us to this state, and
    the total path_cost (also known as g) to reach the node."""

    def __init__(self, state,
                 parent: Optional['Node'] = None,
                 action=None,
                 path_cost: float = 0):
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = path_cost
        self.depth = 0 if parent is None else parent.depth + 1

    def __str__(self):
        return f'<{self.state}>'

    def __lt__(self, other: 'Node') -> bool:
        return self.path_cost < other.path_cost

    def __len__(self): return self.depth


failure = Node('failure', path_cost=math.inf)  # Indicates an algorithm couldn't find a solution.
cutoff = Node('cutoff', path_cost=math.inf)  # Indicates iterative deepening search was cut off.


def expand(problem: Problem, node: Node) -> Generator[Node, None, None]:
    """Expand a node, generating the children nodes."""
    s = node.state
    for action in problem.actions(s):
        s1 = problem.result(s, action)
        cost = node.path_cost + problem.action_cost(s, action, s1)
        yield Node(s1, node, action, cost)


def path_actions(node):
    """The sequence of actions to get to this node."""
    if node.parent is None:
        return []
    return path_actions(node.parent) + [node.action]


def path_states(node):
    """The sequence of states to get to this node."""
    if node in (cutoff, failure, None):
        return []
    return path_states(node.parent) + [node.state]


def path_action_iter(node: Node) -> Generator[str, None, None]:
    """
    Returns a generator of states and actions (as strings), from goal state to initial
    node in reverse order
    :param node:    a node containing a goal state
    :return:        a generator funciton yielding an alternating sequence of states
                    and actions
    """
    n = node
    while True:
        yield f'state: {n}'
        if n.action:
            yield f'action: {n.action}'

        if not n.parent:
            break
        else:
            n = n.parent


def path_states_iter(node: Node) -> Generator[Node, None, None]:
    """
    Returns a generator of states from goal state to initial node in reverse order
    :param node:  a node containing a goal state
    :return:      a generator funciton yielding nodes of the path from goal state
    """
    n = node
    while True:
        yield n

        if not n.parent:
            break
        else:
            n = n.parent
