from search.search_objects import Problem, Node, expand, path_action_iter
from itertools import combinations

from clasic_search import best_first

State = list[int, int, int, int, int, int, int, int, int]


def inversions(board):
    """The number of times a piece is a smaller number than a following piece."""
    return sum((a > b != 0 and a != 0) for (a, b) in combinations(board, 2))


def hamming_distance(p_a, p_b):
    """Number of positions where vectors A and B are different."""
    return sum(a != b for a, b in zip(p_a, p_b))


class EightPuzzle(Problem):
    """ The problem of sliding tiles numbered from 1 to 8 on a 3x3 board,
    where one of the squares is a blank, trying to reach a goal configuration.
    A board state is represented as a tuple of length 9, where the element at index i
    represents the tile number at index i, or 0 if for the empty square, e.g. the goal:
        1 2 3
        4 5 6 ==> [1, 2, 3, 4, 5, 6, 7, 8, 0]
        7 8 _
    """

    def __init__(self, initial: State):
        goal = [1, 2, 3, 4, 5, 6, 7, 8, 0]
        assert inversions(initial) % 2 == inversions(goal) % 2  # Parity check
        super().__init__(initial, goal)

    def actions(self, state: State) -> tuple:
        """The indexes of the squares that the blank can move to."""
        moves = ((1, 3), (0, 2, 4), (1, 5),
                 (0, 4, 6), (1, 3, 5, 7), (2, 4, 8),
                 (3, 7), (4, 6, 8), (7, 5))
        blank = state.index(0)
        return moves[blank]

    def result(self, state: State, action: int) -> State:
        """Swap the blank with the square numbered `action`."""
        blank = state.index(0)
        s1 = state[:]
        s1[action], s1[blank] = s1[blank], s1[action]
        return s1

    def h1(self, node) -> int:
        """The misplaced tiles heuristic."""
        return hamming_distance(node.state, self.goal)

    def h2(self, node: Node) -> int:
        """The Manhattan heuristic."""
        X = (0, 1, 2, 0, 1, 2, 0, 1, 2)
        Y = (0, 0, 0, 1, 1, 1, 2, 2, 2)
        return sum(abs(X[s] - X[g]) + abs(Y[s] - Y[g])
                   for (s, g) in zip(node.state, self.goal) if s != 0)

    def h(self, node: Node) -> int:
        return self.h2(node)


e1 = EightPuzzle([1, 4, 2, 0, 7, 5, 3, 6, 8])
e2 = EightPuzzle([1, 2, 3, 4, 5, 6, 7, 8, 0])
e3 = EightPuzzle([4, 0, 2, 5, 1, 3, 7, 8, 6])
e4 = EightPuzzle([7, 2, 4, 5, 0, 6, 8, 3, 1])
e5 = EightPuzzle([1, 2, 3, 4, 5, 6, 0, 7, 8])

initial_node = Node(e1.initial)
e1.is_goal(e1.initial)
g = expand(e1, initial_node)
list_nodes = list(g)
print(list_nodes[0])
g2 = expand(e1, list_nodes[0])
list_nodes_2 = list(g2)


reached, sln = best_first.breadth_first_bfs(e5)

path = list(path_action_iter(sln))
path.reverse()
print(path)



