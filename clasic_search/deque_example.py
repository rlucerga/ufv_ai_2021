# Example from https://www.tutorialspoint.com/python/python_deque.htm

import collections
# Create a deque
double_ended = collections.deque(["Mon", "Tue", "Wed"])
print (double_ended)

# Append to the right
print("Adding to the right: ")
double_ended.append("Thu")
print (double_ended)

# append to the left
print("Adding to the left: ")
double_ended.appendleft("Sun")
print (double_ended)

# Remove from the right
print("Removing from the right: ")
double_ended.pop()
print (double_ended)

# Remove from the left
print("Removing from the left: ")
double_ended.popleft()
print (double_ended)

# Reverse the dequeue
print("Reversing the deque: ")
double_ended.reverse()
print (double_ended)