# A short script to show how nodes are instantiated, added to the frontier
# and expanded.

from clasic_search import queen_problem

from collections import deque
from search.search_objects import Node, expand

FIFOQueue = deque

# We create an instance of the problem
problem = queen_problem.NQueensProblem(8)

# s = problem.initial
# a1 = problem.actions(s)
# s = problem.result(s, a1[0])

# Root node, with the initial state taken from the problem
node = Node(problem.initial)

# Is this state a goal?
problem.is_goal(problem.initial)

# Crete a frontier as a FIFO queue with the root node
frontier = FIFOQueue([node])

# Add the root node to the preached set, with all nodes that have been reached
reached = set(problem.initial)

# Pop the frontier to get the next node
node = frontier.pop()

# Create a children generator iterator using the generator function expand
children_iterator = expand(problem, node)

# childrent generator will generate actions given the state
child = next(children_iterator)