import math
import sys
from typing import Callable
from queue import PriorityQueue
from search.search_objects import Node, expand, failure, Problem


def best_first_search(problem: Problem, f: Callable[[Node], float]) -> tuple[dict, Node]:
    """
    Search nodes with minimum f(node) value first.
    :param problem: a problem to solve
    :param f:       a function applied to a Node, returning a cost.
                    Lowest cost paths will be expanded first
    :return:        a child node with the solution, or failure
    """

    node = Node(problem.initial)
    # PriorityQueue: A queue in which the item with minimum f(item) is always popped first.
    # Introduce items as tuples of (priority, item)
    frontier = PriorityQueue(maxsize=0)
    frontier.put((f(node), node))
    reached = {tuple(problem.initial): node}
    while not frontier.empty():
        node = frontier.get()[1]
        if problem.is_goal(node.state):
            return reached, node
        for child in expand(problem, node):
            s = tuple(child.state)
            if s not in reached or child.path_cost < reached[s].path_cost:
                reached[s] = child
                frontier.put((f(child), child))
    return dict({}), failure


def astar_search(problem, h=None):
    """Search nodes with minimum f(n) = g(n) + h(n)."""
    h = h or problem.h
    return best_first_search(problem, f=lambda n: g(n) + h(n))


def breadth_first_bfs(problem):
    """Search shallowest nodes in the search tree first; using best-first."""
    return best_first_search(problem, f=lambda n: n.depth)


def depth_first_bfs(problem):
    """Search deeper nodes in the search tree first; using best-first."""
    return best_first_search(problem, f=lambda n: -n.depth)


def g(n: Node):
    return n.path_cost


def uniform_cost_search(problem):
    """Search nodes with minimum path cost first."""
    return best_first_search(problem, f=g)


def greedy_bfs(problem, h=None):
    """Search nodes with minimum h(n)."""
    h = h or problem.h
    return best_first_search(problem, f=h)


def is_cycle(node, k=30):
    """Does this node form a cycle of length k or less?"""

    def find_cycle(ancestor, k):
        return (ancestor is not None and k > 0 and
                (ancestor.state == node.state or find_cycle(ancestor.parent, k - 1)))

    return find_cycle(node.parent, k)


def iterative_deepening_search(problem):
    """Do depth-limited search with increasing depth limits."""
    for limit in range(1, sys.maxsize):
        result = depth_limited_search(problem, limit)
        if result != cutoff:
            return result


LIFOQueue = list
cutoff = Node('cutoff', path_cost=math.inf)


def depth_limited_search(problem, limit=10):
    """Search deepest nodes in the search tree first."""
    frontier = LIFOQueue([Node(problem.initial)])
    result = failure
    while frontier:
        node = frontier.pop()
        if problem.is_goal(node.state):
            return node
        elif len(node) >= limit:
            result = cutoff
        elif not is_cycle(node):
            for child in expand(problem, node):
                frontier.append(child)
    return result
