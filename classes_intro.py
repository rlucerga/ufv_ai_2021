# A simple class

class Complex:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart


x = Complex(3.0, -4.5)
print(x.r, x.i)


# A Python program to demonstrate inheritance

class Person(object):

    # Constructor
    def __init__(self, name):
        self.name = name

    def get_name(self):
        # To get name
        return self.name

    def is_employee(self):
        # To check if this person is employee
        return False


# Inherited or Sub class (Note Person in bracket)
class Employee(Person):

    def is_employee(self):
        # Here we return true
        return True


# Driver code
emp = Person("Geek1")  # An Object of Person
print(emp.get_name(), emp.is_employee())

emp = Employee("Geek2")  # An Object of Employee
print(emp.get_name(), emp.is_employee())