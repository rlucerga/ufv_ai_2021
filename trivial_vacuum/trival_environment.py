from typing import Callable
import random

loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world


class Agent:
    def __init__(self, program: Callable, name: str = "default"):
        self.name = name
        self.alive = True
        self.location = None
        self.performance = 0
        self.program = program


class TrivialVacuumEnvironment:
    """This environment has two locations, A and B. Each can be Dirty
    or Clean. The agent perceives its location and the location's
    status. This serves as an example of how to implement a simple
    Environment."""

    def __init__(self):
        self.agent = None
        self.status = {loc_A: random.choice(['Clean', 'Dirty']),
                       loc_B: random.choice(['Clean', 'Dirty'])}

    @staticmethod
    def default_location():
        """Agents start in either location at random."""
        return random.choice([loc_A, loc_B])

    def add_agent(self, agent, location=None):
        agent.location = location if location else self.default_location()
        agent.performance = 0
        self.agent = agent

    def percept(self, agent):
        """Returns the agent's location, and the location status (Dirty/Clean)."""
        return agent.location, self.status[agent.location]

    def execute_action(self, agent, action):
        """Change agent's location and/or location's status; track performance.
        Score 10 for each dirt cleaned; -1 for each move."""
        if action == 'Right':
            agent.location = loc_B
            agent.performance -= 1
        elif action == 'Left':
            agent.location = loc_A
            agent.performance -= 1
        elif action == 'Suck':
            if self.status[agent.location] == 'Dirty':
                agent.performance += 10
            self.status[agent.location] = 'Clean'

    def step(self):
        """Run the environment for one time step. """
        action = self.agent.program(self.percept(self.agent))
        print(f'agent {self.agent.name} performing action {action}')
        self.execute_action(self.agent, action)

    def render_state(self):
        state = f'agent {self.agent.name} in {self.agent.location}, status: {self.status}'
        print(state)
        return state

    def run(self, steps=1000):
        """Run the Environment for given number of time steps."""
        for step in range(steps):
            self.step()
            self.render_state()
