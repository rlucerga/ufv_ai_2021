loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world


def model_based_vacuum_agent_program():
    """Same as ReflexVacuumAgent, except if everything is clean, do NoOp."""
    model = {loc_A: None, loc_B: None}

    def program(percept):
        location, status = percept
        model[location] = status  # Update the model here
        if model[loc_A] == model[loc_B] == 'Clean':
            return 'NoOp'
        elif status == 'Dirty':
            return 'Suck'
        elif location == loc_A:
            return 'Right'
        elif location == loc_B:
            return 'Left'

    return program