from trivial_vacuum.trival_environment import Agent, TrivialVacuumEnvironment
import random

loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world

def random_agent_program(precept: tuple):
    """
    A random agent program, returning a random action
    """
    if precept:
        return random.choice(['Right', 'Left', 'Suck', 'NoOp'])


if __name__ == '__main__':
    print(random_agent_program((loc_A, 'Clean')))
    print(random_agent_program((loc_A, 'Clean')))
    a = Agent(name='r2p2', program=random_agent_program)
    env = TrivialVacuumEnvironment()
    print('end')
