loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world


class ModelBasedVacuumProgram:
    """Same as ReflexVacuumAgent, except if everything is clean, do NoOp."""

    def __init__(self):
        self.model = {loc_A: None, loc_B: None}

    def __call__(self, percept):
        location, status = percept
        # Update the model here
        self.model[location] = status
        # Use model to take action
        if self.model[loc_A] == self.model[loc_B] == 'Clean':
            return 'NoOp'
        elif status == 'Dirty':
            return 'Suck'
        elif location == loc_A:
            return 'Right'
        elif location == loc_B:
            return 'Left'


if __name__ == '__main__':
    table_driven_agenet_program = ModelBasedVacuumProgram()
    print(table_driven_agenet_program((loc_A, 'Clean')))
    print(table_driven_agenet_program((loc_B, 'Dirty')))
