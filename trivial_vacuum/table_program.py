from trivial_vacuum.trival_environment import TrivialVacuumEnvironment, Agent

loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world

TABLE_1 = {((loc_A, 'Clean'),): 'Right',
         ((loc_A, 'Dirty'),): 'Suck',
         ((loc_B, 'Clean'),): 'Left',
         ((loc_B, 'Dirty'),): 'Suck',
         ((loc_A, 'Dirty'), (loc_A, 'Clean')): 'Right',
         ((loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
         ((loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck',
         ((loc_B, 'Dirty'), (loc_B, 'Clean')): 'Left',
         ((loc_A, 'Dirty'), (loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
         ((loc_B, 'Dirty'), (loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck'}

TABLE_2 = {((loc_A, 'Clean'),): 'Left',
           ((loc_A, 'Dirty'),): 'Suck',
           ((loc_B, 'Clean'),): 'Left',
           ((loc_B, 'Dirty'),): 'Suck',
           ((loc_A, 'Dirty'), (loc_A, 'Clean')): 'Right',
           ((loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
           ((loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck',
           ((loc_B, 'Dirty'), (loc_B, 'Clean')): 'Left',
           ((loc_A, 'Dirty'), (loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
           ((loc_B, 'Dirty'), (loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck'}


class TableDrivenAgentProgram:
    """
    [Figure 2.7]
    This agent selects an action based on the percept sequence.
    It is practical only for tiny domains.
    To customize it, provide as table a dictionary of all
    {percept_sequence:action} pairs.
    """

    def __init__(self, table: dict):
        self.table = table
        self.percepts = []

    def __call__(self, percept: tuple):
        self.percepts.append(percept)
        action = self.table.get(tuple(self.percepts))
        return action


if __name__ == '__main__':
    # Run this with a debugger. Analyze how the objects are instantiated and

    # Instantiate a function (callable) with table 1
    prog_table_1 = TableDrivenAgentProgram(TABLE_1)
    # Instantiate a Agent object, with name agent_1 and agent program prog_table_1
    a_1 = Agent(prog_table_1, name='agent_1')

    # Check how the agent program responds to tuples of observations
    a_1.program((loc_A, "Dirty"))
    a_1.program((loc_A, "Clean"))
    a_1.program((loc_B, "Dirty"))

    # See how the agent is building "memory" of the sequence of percepts
    print('Sequence of percepts')
    print(a_1.program.percepts)

    prog_table_2 = TableDrivenAgentProgram(TABLE_2)
    a_2 = Agent(prog_table_2, name='agent_2')

    triv_env = TrivialVacuumEnvironment()
    triv_env.add_agent(a_2)

    # We start here....
    print('Starting environment state')
    triv_env.render_state()


    triv_env.run(2)

    # We end here....
    print('Ending environment state')
    triv_env.render_state()

