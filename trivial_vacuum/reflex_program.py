loc_A, loc_B = (0, 0), (1, 0)  # The two locations for the Vacuum world


def simple_reflex_agent_program(percept):
    location, status = percept
    if status == 'Dirty':
        return 'Suck'
    elif location == loc_A:
        return 'Right'
    elif location == loc_B:
        return 'Left'


if __name__ == '__main__':
    print(simple_reflex_agent_program((loc_A, 'Clean')))
    print(simple_reflex_agent_program((loc_B, 'Dirty')))