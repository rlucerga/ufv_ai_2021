from typing import Callable
import sys
import random
import numpy as np
from search.search_objects import Node, expand, Problem


def probability(p: float) -> bool:
    """Return true with probability p."""
    return p > random.uniform(0.0, 1.0)


def exp_schedule(k: float = 20., lam: float = 0.005, limit: int = 100) -> Callable[[float], float]:
    """One possible schedule function for simulated annealing"""
    return lambda t: (k * np.exp(-lam * t) if t < limit else 0)


def simulated_annealing(problem: Problem,
                        schedule: Callable[[float], float] = exp_schedule()):
    current = Node(problem.initial)
    for t in range(sys.maxsize):
        T = schedule(t)
        if T == 0:
            return current.state
        neighbors = list(expand(problem, current))
        if not neighbors:
            return current.state
        next_choice = random.choice(neighbors)
        delta_e = problem.value(next_choice.state) - problem.value(current.state)
        if delta_e > 0 or probability(np.exp(delta_e / T)):
            current = next_choice


def simulated_annealing_full(problem: Problem,
                             schedule: Callable[[float], float] = exp_schedule()):
    """ This version returns all the states encountered in reaching
    the goal state. Expand as a generator function returning a random neighbour."""
    states_actions = []
    current = Node(problem.initial)
    for t in range(sys.maxsize):
        states_actions.append(current.state)
        T = schedule(t)
        if T == 0:
            return states_actions
        neighbors = list(expand(problem, current))
        if not neighbors:
            return current.state
        next_choice = random.choice(neighbors)
        delta_e = problem.value(next_choice.state) - problem.value(current.state)
        if delta_e > 0 or probability(np.exp(delta_e / T)):
            current = next_choice
            states_actions.append(current.action)
