from dataclasses import dataclass

# Consider the following grid worlds
grid_world = [
    [3, 2, 0, 1, 4],
    [2, 0, 2, 8, 10],
    [0, 0, 2, 4, 12],
    [0, 2, 4, 8, 16],
    [1, 2, 8, 16, 32],
]

# And this simplified version of Node, including the state only


@dataclass
class Node:
    state: list

# Imagine that we are in (0,0). Consider the 8 directions. Expand returns following sequence of nodes


neighbors = [Node([0, 1]), Node([1, 0]), Node([1, 1])]


max(neighbors, key=lambda node: grid_world[node.state[0]][node.state[1]])