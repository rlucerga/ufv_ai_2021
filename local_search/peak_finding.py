import operator
from typing import Callable

import numpy as np

from local_search.hill_climbing import hill_climbing
from local_search.sim_annealing import simulated_annealing_full, simulated_annealing
from search.search_objects import Problem

# Pre-defined actions for PeakFindingProblem

# If we instantiate the problem with directions4, the algorithm can only changes state moving
# vertically and horizontally. If we use directions8, the the algorithm can also "move" diagonally.
directions4 = {'W': (-1, 0), 'N': (0, 1), 'E': (1, 0), 'S': (0, -1)}
directions8 = dict(directions4)
directions8.update({'NW': (-1, 1), 'NE': (1, 1), 'SE': (1, -1), 'SW': (-1, -1)})


def vector_add(a: tuple, b: tuple) -> tuple:
    """Component-wise addition of two vectors."""
    return tuple(map(operator.add, a, b))


class PeakFindingProblem(Problem):
    """Problem of finding the highest peak in a limited grid"""

    def __init__(self, initial: tuple[int, int], grid: list, defined_actions: dict):
        """The grid is a 2 dimensional array/list whose state is specified by tuple of indices"""
        super().__init__(initial)
        self.grid = grid
        self.defined_actions = defined_actions
        self.n = len(grid)
        assert self.n > 0
        self.m = len(grid[0])
        assert self.m > 0

    def actions(self, state: tuple[int, int]) -> list:
        """Returns the list of actions which are allowed to be taken from the given state"""
        allowed_actions = []
        for action in self.defined_actions:
            next_state = vector_add(state, self.defined_actions[action])
            if 0 <= next_state[0] <= self.n - 1 and 0 <= next_state[1] <= self.m - 1:
                allowed_actions.append(action)

        return allowed_actions

    def result(self, state: tuple[int, int], action: str) -> tuple:
        """Moves in the direction specified by action"""
        return vector_add(state, self.defined_actions[action])

    def value(self, state: tuple) -> int:
        """Value of a state is the value it is the index to"""
        x, y = state
        assert 0 <= x < self.n
        assert 0 <= y < self.m
        return self.grid[x][y]


# This grid defines states (a 5x5 grid, and values for each state)

grid_world = [
    [5, 2, 0, 1, 4],
    [2, 0, 2, 8, 10],
    [0, 0, 2, 4, 12],
    [0, 2, 4, 8, 16],
    [1, 2, 8, 16, 32],
]

# We define the initial position
initial_position = (0, 0)

# And we instantiate the problem
problem = PeakFindingProblem(initial_position, grid_world, directions8)

# PeakFindingProblem objects exposes methods required for problem optimization

# Legal actions from state (0, 0)
problem.actions((0, 0))

# The value of state (0, 0)
problem.value((0, 0))

# The transition model. We can get the s' if we are in s and take action a
problem.result((0, 0), 'N')


# We define a schedule that controls exploration
def exp_schedule(k: float = 5., lam: float = 0.05, limit: int = 40) -> Callable[[float], float]:
    return lambda t: (k * np.exp(-lam * t) if t < limit else 0)


# This first call just performs simulated annealing
sln = simulated_annealing(problem, exp_schedule(5, 0.05, 40))
print(sln)

# simulated_annealing_full runs simulated annealing, but stores in a list states and actions
# as the algorithm explores
sln = simulated_annealing_full(problem, exp_schedule(5, 0.05, 40))
print(sln)

# This is just regular hill climbing
sln = hill_climbing(problem)
print(sln)
