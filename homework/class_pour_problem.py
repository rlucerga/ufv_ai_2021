"""In a water pouring problem you are given a collection of jugs, each of which has a size
(capacity) in, say, litres, and a current level of water (in litres). The goal is to measure out a
certain level of water; it can appear in any of the jugs. For example, in the movie Die Hard 3,
the heroes were faced with the task of making exactly 4 gallons from jugs of size 5 gallons and 3
gallons.)
 
A state is represented by a tuple of current water levels, and the available actions are:

(Fill, i): fill the ith jug all the way to the top (from a tap with unlimited water). (Dump,
i): dump all the water out of the ith jug. (Pour, i, j): pour water from the ith jug into the jth
jug until either the jug i is empty, or jug j is full, whichever comes first. """

from collections import deque
import math


class Problem:
    """The abstract class for a formal problem. A new domain subclasses this,
    overriding `actions` and `results`, and perhaps other methods.
    The default heuristic is 0 and the default action cost is 1 for all states.
    When yiou create an instance of a subclass, specify `initial`, and `goal` states
    (or give an `is_goal` method) and perhaps other keyword args for the subclass."""

    def __init__(self, initial=None, goal=None):
        """The constructor specifies the initial state, and possibly a goal
        state, if there is a unique goal. Your subclass's constructor can add
        other arguments."""
        self.initial = initial
        self.goal = goal

    def actions(self, state):
        """Return the actions that can be executed in the given
        state. The result would typically be a list, but if there are
        many actions, consider yielding them one at a time in an
        iterator, rather than building them all at once."""
        raise NotImplementedError

    def result(self, state, action):
        """Return the state that results from executing the given
        action in the given state. The action must be one of
        self.actions(state)."""
        raise NotImplementedError

    def is_goal(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal or checks for state in self.goal if it is a
        list, as specified in the constructor. Override this method if
        checking against a single self.goal is not enough."""
        return state == self.goal

    @staticmethod
    def action_cost(s, a, s1):
        """Return the cost of a solution path that arrives at state s1 from
        s via action a. The default method costs 1 for every step in the path."""
        if s and a and s1:
            return 1

    def __str__(self):
        return '{}({!r}, {!r})'.format(
            type(self).__name__, self.initial, self.goal)


Action = tuple[str, int, int]
State = tuple[int, ...]


class PourProblem(Problem):
    """Problem about pouring water between jugs to achieve some water level.
    Each state is a tuples of water levels. In the initialization, also provide a tuple of
    jug sizes, e.g. PourProblem(initial=(0, 0), goal=4, sizes=(5, 3)),
    which means two jugs of sizes 5 and 3, initially both empty, with the goal
    of getting a level of 4 in either jug."""

    def __init__(self, initial: State, goal: int, sizes: tuple[int, ...]):
        super().__init__(initial, goal)
        self.sizes = sizes

    def actions(self, state: State) -> list[Action]:
        """The actions executable in this state."""
        jugs = range(len(state))
        return ([('Fill', i, 0) for i in jugs if state[i] < self.sizes[i]] +
                [('Dump', i, 0) for i in jugs if state[i]] +
                [('Pour', i, j) for i in jugs if state[i] for j in jugs if i != j])

    def result(self, state: State, action: Action) -> State:
        """The state that results from executing this action in this state."""
        result = list(state)
        act, i, j = action
        if act == 'Fill':  # Fill i to capacity
            result[i] = self.sizes[i]
        elif act == 'Dump':  # Empty i
            result[i] = 0
        elif act == 'Pour':  # Pour from i into j
            amount = min(state[i], self.sizes[j] - state[j])
            result[i] -= amount
            result[j] += amount
        return tuple(result)

    def is_goal(self, state) -> bool:
        """True if the goal level is in any one of the jugs."""
        return self.goal in state


class Node:
    """A node in a search tree. Contains a pointer to the parent (the node
    that this is a successor of) and to the actual state for this node. Note
    that if a state is arrived at by two paths, then there are two nodes with
    the same state. Also includes the action that got us to this state, and
    the total path_cost (also known as g) to reach the node."""

    def __init__(self, state,
                 parent=None,
                 action=None,
                 path_cost: float = 0):
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = path_cost

    def __repr__(self):
        return f'<{self.state}>'

    def __len__(self):
        return 0 if self.parent is None else (1 + len(self.parent))

    def __lt__(self, other):
        return self.path_cost < other.path_cost


failure = Node('failure', path_cost=math.inf)  # Indicates an algorithm couldn't find a solution.
cutoff = Node('cutoff', path_cost=math.inf)  # Indicates iterative deepening search was cut off.


def expand(problem: Problem, node: Node):
    """Expand a node, generating the children nodes."""
    s = node.state
    for action in problem.actions(s):
        s1 = problem.result(s, action)
        cost = node.path_cost + problem.action_cost(s, action, s1)
        yield Node(s1, node, action, cost)


def path_actions(node: Node):
    """The sequence of actions to get to this node."""
    if node.parent is None:
        return []
    return path_actions(node.parent) + [node.action]


def path_states(node: Node):
    """The sequence of states to get to this node."""
    if node in (cutoff, failure, None):
        return []
    return path_states(node.parent) + [node.state]


FIFOQueue = deque


def breadth_first_search(problem: Problem):
    """Search shallowest nodes in the search tree first."""
    node = Node(problem.initial)
    if problem.is_goal(problem.initial):
        return node
    frontier = FIFOQueue([node])
    reached = set(problem.initial)
    while frontier:
        node = frontier.pop()
        for child in expand(problem, node):
            s = child.state
            if problem.is_goal(s):  
                return child
            if s not in reached:
                reached.add(s)
                frontier.appendleft(child)
    return failure
