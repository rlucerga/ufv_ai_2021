from typing import Optional, Callable

import random
import math
import functools

cache = functools.lru_cache(10 ** 6)

# Squares or moves of each player, coordinate x and y, origin top left
Square = tuple[int, int]


class Game:
    """A game is similar to a problem, but it has a terminal test instead of
    a goal test, and a utility for each terminal state. To create a game,
    subclass this class and implement `actions`, `result`, `is_terminal`,
    and `utility`. You will also need to set the .initial attribute to the
    initial state; this can be done in the constructor."""

    def __init__(self, initial):
        self.initial = initial

    def actions(self, state):
        """Return a collection of the allowable moves from this state."""
        raise NotImplementedError

    def result(self, state, move):
        """Return the state that results from making a move from a state."""
        raise NotImplementedError

    def is_terminal(self, state):
        """Return True if this is a final state for the game."""
        return not self.actions(state)

    def utility(self, state, player):
        """Return the value of this final state to player."""
        raise NotImplementedError

    @staticmethod
    def display(state):
        """Render a representation of the game based on the state."""
        raise NotImplementedError


class Board(dict):
    """We use Board to represent states in Tic Tac Toe.
    A board has the player to move, a cached utility value and a dict of {(x, y): player} entries,
    where player is 'X' or 'O'."""
    empty = '.'
    off = '#'

    def __init__(self, width: int = 3, height: int = 3, to_move: str = None, utility: int = 0):
        super().__init__()
        self.width = width
        self.height = height
        self.to_move = to_move
        self.utility = utility

    def new(self, changes: dict, to_move: str) -> 'Board':
        """Given a dict of {(x, y): contents} changes, return a new Board with the changes."""
        board = Board(width=self.width, height=self.height, to_move=to_move)
        board.update(self)
        board.update(changes)
        return board

    def __missing__(self, loc: tuple[int, int]) -> str:
        x, y = loc
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.empty
        else:
            return self.off

    def __hash__(self):
        return hash(tuple(sorted(self.items()))) + hash(self.to_move)

    def __str__(self) -> str:
        def row(y): return ' '.join(self[x, y] for x in range(self.width))

        return '\n'.join(map(row, range(self.height))) + '\n'


class TicTacToe(Game):
    """Play TicTacToe on an `height` by `width` board, needing `k` in a row to win.
    'X' plays first against 'O'."""

    def __init__(self, height: int = 3, width: int = 3, k: int = 3):
        initial = Board(height=height, width=width, to_move='X', utility=0)
        super().__init__(initial)
        self.k = k  # k in a row
        self.squares = {(x, y) for x in range(width) for y in range(height)}

    def actions(self, board: Board) -> set[Square]:
        """Legal moves are any square not yet taken."""
        return self.squares - set(board)

    def result(self, board: Board, square: Square) -> Board:
        """Place a marker for current player on square."""
        player = board.to_move
        board = board.new({square: player}, to_move=('O' if player == 'X' else 'X'))
        win = self.k_in_row(board, player, square, self.k)
        board.utility = (0 if not win else +1 if player == 'X' else -1)
        return board

    def utility(self, board: Board, player: str) -> int:
        """Return the value to player; 1 for win, -1 for loss, 0 otherwise."""
        return board.utility if player == 'X' else -board.utility

    def is_terminal(self, board: Board) -> bool:
        """A board is a terminal state if it is won or there are no empty squares."""
        return board.utility != 0 or len(self.squares) == len(board)

    @staticmethod
    def display(board: Board): print(board)

    @staticmethod
    def k_in_row(board: Board, player: str, square: tuple[int, int], k: int) -> bool:
        """True if player has k pieces in a line through square."""

        def in_row(x, y, dx, dy):
            return 0 if board[x, y] != player else 1 + in_row(x + dx, y + dy, dx, dy)

        return any(in_row(*square, dx, dy) + in_row(*square, -dx, -dy) - 1 >= k
                   for (dx, dy) in ((0, 1), (1, 0), (1, 1), (1, -1)))


def random_player(game: Game, in_state: Board) -> Square:
    return random.choice(list(game.actions(in_state)))


def minimax_search(game: Game, current_state: Board) -> tuple[float, Optional[Square]]:
    """Search game tree to determine best move; return (value, move) pair."""

    player = current_state.to_move

    def max_value(state: Board) -> tuple[float, Optional[Square]]:
        if game.is_terminal(state):
            return game.utility(state, player), None
        v, move = -infinity, None
        for a in game.actions(state):
            v2, _ = min_value(game.result(state, a))
            if v2 > v:
                v, move = v2, a
        return v, move

    def min_value(state: Board) -> tuple[float, Optional[Square]]:
        if game.is_terminal(state):
            return game.utility(state, player), None
        v, move = +infinity, None
        for a in game.actions(state):
            v2, _ = max_value(game.result(state, a))
            if v2 < v:
                v, move = v2, a
        return v, move

    return max_value(current_state)


infinity = math.inf


def alphabeta_search(game: Game, current_state: Board) -> tuple[float, Optional[Square]]:
    """Search game to determine best action; use alpha-beta pruning.
    As in [Figure 5.7], this version searches all the way to the leaves."""

    player = current_state.to_move

    def max_value(state: Board, alpha: float, beta: float) -> tuple[float, Optional[Square]]:
        if game.is_terminal(state):
            return game.utility(state, player), None
        v, move = -infinity, None
        for a in game.actions(state):
            v2, _ = min_value(game.result(state, a), alpha, beta)
            if v2 > v:
                v, move = v2, a
                alpha = max(alpha, v)
            if v >= beta:
                return v, move
        return v, move

    def min_value(state: Board, alpha: float, beta: float) -> tuple[float, Optional[Square]]:
        if game.is_terminal(state):
            return game.utility(state, player), None
        v, move = +infinity, None
        for a in game.actions(state):
            v2, _ = max_value(game.result(state, a), alpha, beta)
            if v2 < v:
                v, move = v2, a
                beta = min(beta, v)
            if v <= alpha:
                return v, move
        return v, move

    return max_value(current_state, -infinity, +infinity)


def query_player(game: Game, state: Board) -> Square:
    """Make a move by querying standard input."""
    print("current state:")
    game.display(state)
    print("available moves: {}".format(game.actions(state)))
    print("")
    move = None
    if game.actions(state):
        move_string = input('Your move? ')
        try:
            move = eval(move_string)
        except NameError:
            move = move_string
    else:
        print('no legal moves: passing turn to next player')
    return move


def new_player(search_algorithm: Callable[[Game, Board], tuple[float, Square]]) \
        -> Callable[[Game, Board], Square]:
    """A game player who uses the specified search algorithm"""
    return lambda game, in_state: search_algorithm(game, in_state)[1]


def play_game(game: Game,
              strategies: dict[str, Callable[[Game, Board], Square]],
              verbose: bool = False) -> Board:
    """Play a turn-taking game. `strategies` is a {player_name: function} dict,
    where function(game, state) is used to get the player's move."""
    state = game.initial
    while not game.is_terminal(state):
        player = state.to_move
        move = strategies[player](game, state)
        state = game.result(state, move)
        if verbose:
            print('Player', player, 'move:', move)
            print(state)
    return state


def game_1():
    """A game between a random player and alpha beta search agent.
    Alpha beta will never lose, and win most of the times"""
    ttt_game = TicTacToe(height=3, width=3, k=3)

    state = play_game(
        game=ttt_game,
        strategies=dict(X=random_player, O=new_player(alphabeta_search)),
        verbose=True)
    print(state.utility)


def game_2():
    ttt_game = TicTacToe(height=3, width=3, k=3)

    state = play_game(
        game=ttt_game,
        strategies=dict(X=new_player(alphabeta_search), O=query_player),
        verbose=True)
    print(state.utility)


if __name__ == "__main__":
    game_1()
