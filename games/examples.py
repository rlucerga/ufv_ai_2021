from collections import UserDict
from dataclasses import dataclass


class Board(UserDict):
    def __init__(self, to_move: str = None, **kwargs):
        super().__init__(**kwargs)
        self.to_move = to_move


b = Board('O')

b[(0, 1)] = 'X'
b[(1, 1)] = 'O'

set(b.keys())

squares = {(i, j) for i in range(2) for j in range(2)}

u = set()
for i in range(2):
    for j in range(2):
        u.add((i, i))

squares - set(b.keys())

t = """
a 1
b 2
"""

import  re

re.match('\D', t, re.MULTILINE)

m = re.search(r'(\d+)','3432 dkdfa 35345',)

t = "'ñafdjdalfj"

re.fullmatch(r"'\w+",t)