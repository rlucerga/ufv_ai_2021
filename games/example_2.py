from collections import UserDict


class Board(UserDict):
    def __init__(self, w: int = 3, h: int = 3, to_move: str = 'x'):
        super().__init__({'x': [], 'o': []})
        self.to_move = to_move
        self.h = h
        self.w = w
        self.squares = {(i, j) for i in range(w) for j in range(h)}

    def legal_moves(self) -> set[tuple[int, int]]:
        moves = {coord for coord_list in self.values() for coord in coord_list}
        return self.squares - moves

    def mark(self, coord: tuple[int, int]):
        self[self.to_move].append(coord)
        if self.to_move == 'x':
            self.to_move = 'o'
        else:
            self.to_move = 'x'


b = Board(h=4, w=3, to_move='o')
b.mark((0, 0))



