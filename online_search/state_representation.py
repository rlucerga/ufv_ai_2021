
# A simple single state representation

one_dim_state_space = {
    'State_1': {'Right': 'State_2'},
    'State_2': {'Right': 'State_3', 'Left': 'State_1'},
    'State_3': {'Right': 'State_4', 'Left': 'State_2'},
    'State_4': {'Right': 'State_5', 'Left': 'State_3'},
    'State_5': {'Right': 'State_6', 'Left': 'State_4'},
    'State_6': {'Left': 'State_5'}
}

# Partial representation of the maize

inner_11 = {'N': (1, 2),
            'E': (2, 1)}

inner_12 = {'S': (1, 1)}

d: dict[tuple[int, int], dict[str, tuple[int, int]]] = {(1, 1): inner_11,
                                                        (1, 2): inner_12}

# For example, actions from state (1, 2)
list(d[(1, 2)].keys())


# Full implemenation of the dictionary

graph_dict = {
    (1, 1): {'N': (1, 2), 'E': (2, 1)},
    (2, 1): {'W': (1, 1), 'N': (2, 2), 'E': (3, 1)},
    (3, 1): {'W': (2, 1), 'N': (3, 2)},
    (1, 2): {'S': (1, 1)},
    (2, 2): {'N': (2, 3), 'S': (2, 1)},
    (3, 2): {'N': (3, 3), 'S': (3, 1)},
    (1, 3): {'E': (2, 3)},
    (2, 3): {'W': (1, 3), 'S': (2, 2)},
    (3, 3): {'S': (3, 2)}
}

# Get dictionary of state actions if agent is in location (1, 1)
print(graph_dict.get((1, 1)))

# Get actions in state (1, 1)
print(graph_dict.get((1, 1)).keys())

# New state when taking action N from state (1, 1=
print(graph_dict.get((1, 1)).get('N'))





