from typing import Callable, Optional
import numpy as np

SIZE = 4

# Type alias
State = tuple[int, int]
Action = str
Heuristic = dict[State, int]
GraphDict = dict[State, dict[Action, State]]


class LRTAStarAgent:
    """
    Abstract class for LRTA*-Agent.

    This is an online search agent. The agent maintains H(s), the current best estimate
    of the cost to reach the goal from each state that has visited. H(s) starts out being
    just h(s), a heuristic of the cost estimate passed as argument.
    Action is chosen by the agent program by choosing from actions(s) the action that
    minmizes cost from the current state, calculated by LRTA_cost.

    For fully observable, safely explorable environment. The agent can work with unkown
    environments and only needs to be able to explore locally (i.e. actions and states 
    resulting from those actions neighbouring the current state are known and can be
    expanded).

    Modified version from online repository [Norving and Russel 2009]

    For more details see [Norving and Russel 2009]

    Norving and Russel, "Artificial Intelligence Modern Approach", 2009.
    """

    def __init__(self,
                 actions: Callable[[State], list[Action]] = None,
                 expand: Callable[[State], dict[Action, State]] = None,
                 goal_test: Callable[[State], bool] = None,
                 h: Callable[[State], int] = None,
                 c: Callable[[State, Action, State], int] = None):

        """
        Constructor for LRTAStarAgent class

        Note that the agent does not have access to the result function, but builds its own
        result table as it explores.

        :param actions:     a function that returns the actions available from a given state
        :param expand:      a function returning a dictionary of actions (keys) and and resulting
                            states as values for a given state
        :param goal_test:   function returning True if the state is a goal state
        :param h:           heuristic for the A* algorithm
        :param c:           cost function, return the cost from state s to state s' given action a
        
        :returns: an LRTAStarAgent class object
        """
        self.performance = 0
        self.actions = actions
        self.goal_test = goal_test
        self.expand = expand
        self.h = h
        self.c = c

        self.result = {}
        self.H = {}
        self.s = None
        self.a = None

    def reset(self):
        self.performance = 0
        self.result = {}
        self.H = {}
        self.s = None
        self.a = None

    def program(self, s1: State) -> Optional[Action]:
        """
        Agent program, receives a percept and returns an action.
        As the environment is fully observable, percept = state
        """
        if self.goal_test(s1):
            self.a = None
            return self.a
        else:
            if s1 not in self.H:
                self.H[s1] = self.h(s1)
            if self.s is not None:
                # minimum cost for action b in from actions(s)
                self.H[self.s] = min(self.LRTA_cost(self.s, b, self.result[self.s, b])
                                     for b in self.actions(self.s))

            # the agent expands locally and updates the internal result function
            for b in self.actions(s1):
                self.result.update({(s1, b): self.expand(s1)[b]})

            # an action b in actions(s1) that minimizes costs
            self.a = min(self.actions(s1),
                         key=lambda b: self.LRTA_cost(s1, b, self.result[s1, b]))

            self.s = s1
            self.render_h()
            self.render_H()
            return self.a

    def render_h(self):
        print("\nh:\n")
        print_function(self.h, SIZE, SIZE)

    def render_H(self):
        print("\nH:\n")
        print_values(self.H, SIZE, SIZE)

    def LRTA_cost(self, s: State, a: Action, s1: State) -> int:
        """Returns cost to move from state 's' to state 's1' plus
        estimated cost to get to goal from s1."""
        if s1 is None:
            return self.h(s)
        else:
            # LRTA is computed using h in s1 not in H yet
            if s1 in self.H:
                return self.c(s, a, s1) + self.H[s1]
            else:
                return self.c(s, a, s1) + self.h(s1)


# ______________________________________________________________________________
class Graph:
    """
    A Graph to store the transition model for an online agent.
    `get` method on a state ``s` returns a dictionary with actions `a` as keys and
    `result(s,a)` as values.
    `get` method on a state ``s` and action `a` returns `result(s,a)`
    """

    def __init__(self,
                 graph_dict: GraphDict = None):
        """
        Constructor for Graph class

        :param graph_dict:  a dictionary where keys are states and values are dictionaries with
                            action-state pairs

        :returns: a Graph class object
        """
        self.graph_dict = graph_dict or {}

    def get(self, a, b=None):
        links = self.graph_dict.setdefault(a, {})
        if b is None:
            return links
        else:
            return links.get(b)


def print_grid(location: State, w: int, h: int):
    """Prints grid of height w and width w, with a marker
    (1,1) is bottom right corner
    """
    grid = [["-" for _ in range(w)] for _ in range(h)]
    grid[h - location[1]][location[0] - 1] = "o"
    print('\n'.join(''.join(row) for row in grid))


def print_values(H: Heuristic, w: int, h: int):
    """Print the grid H.
    H is a dictionary (x,y) tupples as keys"""
    grid = [["-" for _ in range(w)] for _ in range(h)]
    for coord, H_value in H.items():
        grid[h - coord[1]][coord[0] - 1] = str(H_value)
    print('\n'.join(''.join(row) for row in grid))


def print_function(_h: Callable, w: int, h: int):
    """Print the grid H.
    H is a dictionary (x,y) tupples as keys"""
    grid = [[str(_h((x + 1, y + 1))) for x in range(w)] for y in range(h - 1, -1, -1)]
    print('\n'.join(''.join(row) for row in grid))


class OnlineSearchEnvironment:
    """
    An online search environment. Deterministic and a fully observable.
    States, actions and transition model (not known to the agent) are defined
    using a Graph object. The goal is a state.
    """

    def __init__(self, goal: State, graph: Graph):
        """
        Constructor for OnlineSearchEnvironment class

        :param goal:    goal state
        :param graph:   a Graph object with a get method that implements the 
                        transition model
        
        :returns: an OnlineSearchEnvironment class object
        """
        self.goal = goal
        self.graph = graph
        self.agent = None
        self.location = None
        self.initial = None

        self.reset()

    def add_agent(self, agent: LRTAStarAgent, location: State = None):
        """Adds agent to the environment and sets its location"""
        self.initial = location  # store for reset
        self.location = location  # location of the agent in the environment
        self.agent = agent  # slot for the agent
        self.agent.performance = 0  # performance

        """
        We assume the agent knows only:
        - actions
        - step-cost funtion
        - goal test
        """
        self.agent.actions = self.actions
        self.agent.c = self.c
        self.agent.goal_test = self.goal_test

        """
        Additionally, the agent knows:
        - the heuristic function (s)
        - locally, the states neighbouring its current location
        """
        self.agent.expand = self.expand
        self.agent.h = self.h

    def reset(self):
        """Resets the environemnt"""
        if self.agent is not None:
            self.location = self.initial
            self.agent.performance = 0

    def percept(self, agent: LRTAStarAgent):
        """Return the percept of the agent"""
        if agent is not None:
            return self.location

    def result(self, state: State, action: Action) -> State:
        """Transition model, implemented with a Graph object"""
        return self.graph.get(state, action)

    def execute_action(self, action: Action):
        """Change the environment to reflect this action"""
        print("\naction %s\n" % action)
        # insert your code here.

    def expand(self, state: State) -> dict[Action, State]:
        """Return the actions and states that can be executed in the given 
        state."""
        return self.graph.get(state)

    def actions(self, state: State) -> list[Action]:
        """Return the actions that can be executed in the given
        state."""
        # return self.graph.graph_dict[state].keys()
        return list(self.graph.get(state).keys())

    def is_done(self) -> bool:
        """End sequential run"""
        return self.goal_test(self.location)

    def step(self) -> bool:
        """Run the environment for one time step."""

        if not self.is_done():
            action = self.agent.program(self.percept(self.agent))
            if action is not None:
                self.execute_action(action)
                self.render()
                return True
            else:
                print("No solution found")
                return False

    def run(self, steps: int = 1000) -> None:
        """Run the Environment for given number of time steps or is_done"""
        self.render()
        for _ in range(steps):
            if self.is_done():
                return
            next_step = self.step()
            if not next_step:
                return

    def h(self, state: State) -> int:
        """Returns least possible cost to reach a goal for the given state."""
        #insert your code here

    @staticmethod
    def c(s: State, a: Action, s1: State) -> int:
        """Returns a cost an agent to move from state 's' to state 's1'."""
        return 1

    def goal_test(self, state: State) -> bool:
        # insert your code here

    def render(self):
        print_grid(
            location=self.location,
            h=SIZE,
            w=SIZE)
        print("\nperformance %s\n" % self.agent.performance)


if __name__ == '__main__':
    maze = Graph(
        graph_dict={
            (1, 1): {'N': (1, 2), 'E': (2, 1)},
            (2, 1): {'E': (3, 1), 'W': (1, 1)},
            (3, 1): {'E': (4, 1), 'W': (2, 1)},
            (4, 1): {'N': (4, 2), 'W': (3, 1)},
            (1, 2): {'N': (1, 3), 'S': (1, 1)},
            (2, 2): {'N': (2, 3), 'E': (3, 2)},
            (3, 2): {'N': (3, 3), 'E': (4, 2), 'W': (2, 2)},
            (4, 2): {'N': (4, 3), 'S': (4, 1), 'W': (3, 2)},
            (1, 3): {'S': (1, 2)},
            (2, 3): {'N': (2, 4), 'E': (3, 3), 'S': (2, 2)},
            (3, 3): {'E': (4, 3), 'S': (3, 2), 'W': (2, 3)},
            (4, 3): {'S': (4, 2), 'W': (3, 3)},
            (1, 4): {'E': (2, 4)},
            (2, 4): {'E': (3, 4), 'S': (2, 3), 'W': (1, 4)},
            (3, 4): {'E': (4, 4), 'W': (2, 4)},
            (4, 4): {'W': (3, 4)}
        }
    )

    env = OnlineSearchEnvironment((4, 4), maze)

    lrta_agent = LRTAStarAgent()

    env.add_agent(agent=lrta_agent, location=(1, 1))

    env.run(40)
