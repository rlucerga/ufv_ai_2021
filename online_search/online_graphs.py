class Graph:
    """
    A Graph to store the transition model for an online agent.
    `get` method on a state ``s` returns a dictionary with actions `a` as keys and
    `result(s,a)` as values.
    `get` method on a state ``s` and action `a` returns `result(s,a)`
    """

    def __init__(self, graph_dict=None):
        """
        Constructor for Graph class

        :param graph_dict:  a dictionary where keys are states and values are dictionaries with
                            action-state pairs

        :returns: a Graph class object
        """
        self.graph_dict = graph_dict or {}

    def get(self, s, a=None):
        links = self.graph_dict.setdefault(s, {})
        if a is None:
            return links
        else:
            return links.get(s)


one_dim_state_space = Graph({
    'State_1': {'Right': 'State_2'},
    'State_2': {'Right': 'State_3', 'Left': 'State_1'},
    'State_3': {'Right': 'State_4', 'Left': 'State_2'},
    'State_4': {'Right': 'State_5', 'Left': 'State_3'},
    'State_5': {'Right': 'State_6', 'Left': 'State_4'},
    'State_6': {'Left': 'State_5'}
})

maze = Graph(
    graph_dict={
        (1, 1): {'N': (1, 2), 'E': (2, 1)},
        (2, 1): {'W': (1, 1), 'N': (2, 2), 'E': (3, 1)},
        (3, 1): {'W': (2, 1), 'N': (3, 2)},
        (1, 2): {'S': (1, 1)},
        (2, 2): {'N': (2, 3), 'S': (2, 1)},
        (3, 2): {'N': (3, 3), 'S': (3, 1)},
        (1, 3): {'E': (2, 3)},
        (2, 3): {'W': (1, 3), 'S': (2, 2)},
        (3, 3): {'S': (3, 2)}
    }
)

if __name__ == '__main__':
    # Get dictionary of state actions if agent is in location (1, 1)
    print(maze.get((1, 1)))

    # Get actions in state (1, 1)
    print(maze.get((1, 1)).keys())

    # New state when taking action N from state (1, 1=
    print(maze.get((1, 1)).get('N'))
